﻿using Meow.Core;
using Meow.Core.Input;
using Microsoft.Xna.Framework;
using Myra;
using Myra.Graphics2D.UI;
using Myra.Samples.AllWidgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDemo;

internal class UIWidgetsScene : Scene
{
    public readonly string NAME = "UIWidgetsScene";

    private Desktop _desktop;
    AllWidgets _allWidgets;

    Image background;
    Microsoft.Xna.Framework.Color engineClearColor;

    Engine engine;
    public UIWidgetsScene(Engine engine) : base(engine)
    {
        this.engine = engine;
    }

    public override void LoadContents()
    {
        base.LoadContents();

        // use this.Content to load your game content here
        MyraEnvironment.Game = engine;
        MyraEnvironment.EnableModalDarkening = true;
        _desktop = new Desktop();
    }

    public override void BeginRun()
    {
        base.BeginRun();

        Screen.ShowCursor = true;
        InputHandler.LockTheMouse = false;

        engineClearColor = engine.ClearColor;
        engine.ClearColor = new Microsoft.Xna.Framework.Color(0.3f, 0.3f, 0.3f);

        _allWidgets = new AllWidgets();

        //background = new Image();
        //background.Renderable = DefaultAssets.WhiteRegion;
        //background.ZIndex = -1;
        //background.Color = new Microsoft.Xna.Framework.Color(0.3f,0.3f,0.3f);
        //background.Width = Screen.Width;
        //background.Height = Screen.Height;
        //panel.Widgets.Add(background);

        var uiwidgetSceneButton = new ImageTextButton
        {
            Margin = new Myra.Graphics2D.Thickness(10),
            Padding = new Myra.Graphics2D.Thickness(3),
            TextColor = Color.Coral,
            Text = "Main Scene",
            HorizontalAlignment = HorizontalAlignment.Right,
            VerticalAlignment = VerticalAlignment.Top
        };
        uiwidgetSceneButton.TouchDown += (s, a) =>
        {
            SwitchToMain();
        };
        _allWidgets.Widgets.Add(uiwidgetSceneButton);


        //_desktop.TouchDown += (s, a) => ShowContextMenu();
    }

    internal void SwitchToMain()
    {
        var mainScene = new DemoManagerScene(engine);
        engine.AddSceneToHandle(mainScene.NAME, mainScene);

        UnLoad();
    }

    public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
    {
        base.Update(gameTime);

        _desktop.Root = _allWidgets;
        _allWidgets._horizontalProgressBar.Value += 0.5f;
        if (_allWidgets._horizontalProgressBar.Value > _allWidgets._horizontalProgressBar.Maximum)
        {
            _allWidgets._horizontalProgressBar.Value = _allWidgets._horizontalProgressBar.Minimum;
        }

        _allWidgets._verticalProgressBar.Value += 0.5f;
        if (_allWidgets._verticalProgressBar.Value > _allWidgets._verticalProgressBar.Maximum)
        {
            _allWidgets._verticalProgressBar.Value = _allWidgets._verticalProgressBar.Minimum;
        }
    }

    public override void Render(in Microsoft.Xna.Framework.GraphicsDeviceManager gdm, Microsoft.Xna.Framework.GameTime gameTime, int tick)
    {
        base.Render(gdm, gameTime, tick);

        //background.Width = Screen.Width;
        //background.Height = Screen.Height;
        _allWidgets._labelOverGui.Text = "Is mouse over GUI: " + _desktop.IsMouseOverGUI;
        _desktop.Render();
    }

    public void UnLoad()
    {
        ToUnload = true;
        engine.ClearColor = engineClearColor;
        UpdatePause = true;
        TickPause = true;
        RenderPause = true;
    }

    public override void Dispose()
    {
        _desktop?.Dispose();

        base.Dispose();
    }

    private void ShowContextMenu()
    {
        if (_desktop.ContextMenu != null)
        {
            // Dont show if it's already shown
            return;
        }

        var container = new VerticalStackPanel
        {
            Spacing = 4
        };

        var titleContainer = new Panel
        {
            Background = DefaultAssets.UITextureRegionAtlas["button"],
        };

        var titleLabel = new Label
        {
            Text = "Choose Option",
            HorizontalAlignment = HorizontalAlignment.Center
        };

        titleContainer.Widgets.Add(titleLabel);
        container.Widgets.Add(titleContainer);

        var menuItem1 = new MenuItem();
        menuItem1.Text = "Start New Game";
        menuItem1.Selected += (s, a) =>
        {
            // "Start New Game" selected
            SwitchToMain();
        };

        var menuItem2 = new MenuItem();
        menuItem2.Text = "Options";

        var menuItem3 = new MenuItem();
        menuItem3.Text = "Quit";
        menuItem3.Selected += (s, a) =>
        {
            engine.Exit();
        };

        var verticalMenu = new VerticalMenu();

        verticalMenu.Items.Add(menuItem1);
        verticalMenu.Items.Add(menuItem2);
        verticalMenu.Items.Add(menuItem3);

        container.Widgets.Add(verticalMenu);

        _desktop.ShowContextMenu(container, _desktop.TouchPosition.Value);
    }

}
