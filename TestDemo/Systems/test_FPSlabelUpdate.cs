﻿using Arch.Core;
using Microsoft.Xna.Framework;
using Myra.Graphics2D.UI;

namespace Meow.Core.Simulation;

public class test_FPSlabelUpdate : ECS_System, IUpdateSystem
{
    string prefix;
    Label fpslabel;
    public test_FPSlabelUpdate(World world, Scene scene, Label fpsLabel, in string prefix) : base(world, scene)
    {
        this.fpslabel = fpsLabel;
        this.prefix = prefix;
    }

    void IUpdateSystem.Update(GameTime gameTime, float totalElapsedTime, float scaleFactor)
    {
        fpslabel.Text = prefix + Application.Engine.FPS;
    }
}
