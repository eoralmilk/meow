﻿using Arch.Core;
using BepuUtilities;
using System.Numerics;

namespace Meow.Core.Simulation;

internal class test_RotateEverythingTickSystem : ECS_System, ITickSystem
{
    public test_RotateEverythingTickSystem(World world, Scene scene) : base(world, scene)
    {
    }

    QueryDescription rotateQuery = new QueryDescription()
                                .WithAll<C_Rotation>();

    public void Tick(int tick)
    {
        world.Query(in rotateQuery, (ref C_Rotation rot) =>
        {
            rot.Value = Quaternion.CreateFromAxisAngle(Vector3.UnitY, MathHelper.ToRadians(3)) * rot.Value;
        });
    }
}
