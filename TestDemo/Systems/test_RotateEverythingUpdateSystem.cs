﻿using Arch.Core;
using System.Numerics;
using BepuUtilities;

namespace Meow.Core.Simulation;

public class test_RotateEverythingUpdateSystem : ECS_System, IUpdateSystem
{
    public test_RotateEverythingUpdateSystem(World world, Scene scene) : base(world, scene)
    {
    }

    QueryDescription rotateQuery = new QueryDescription()
                                .WithAll<C_Rotation>();

    void IUpdateSystem.Update(Microsoft.Xna.Framework.GameTime gameTime, float totalElapsedTime, float scaleFactor)
    {
        world.Query(in rotateQuery, (ref C_Rotation rot, ref C_Position pos) =>
        {
            pos.Value = new Vector3(0, MathF.Sin(totalElapsedTime), 0);

            rot.Value = Quaternion.CreateFromAxisAngle(Vector3.UnitY, MathHelper.ToRadians(75) * (float)gameTime.ElapsedGameTime.TotalSeconds) * rot.Value;
        });
    }
}
