﻿using Meow.Core;
using Meow.Core.Input;
using Microsoft.Xna.Framework.Input;
using Myra.Graphics2D.UI;
using TestDemo;

public class Demo
{
    static void Main(string[] args)
    {
        using (var game = new Engine("Meow Meow", "./"))
        {
            var mainScene = new DemoManagerScene(game);
            game.AddSceneToHandle(mainScene.NAME, mainScene);

            InputHandler.AddKeyToHandle(Keys.T);
            InputHandler.AllKeysHandling[Keys.T].KeyDownActions.Add(() => Screen.Fullscreen = !Screen.Fullscreen);

            InputHandler.AddKeyToHandle(Keys.C);
            InputHandler.AllKeysHandling[Keys.C].KeyDownActions.Add(ToggleCursorHide);

            InputHandler.AddKeyToHandle(Keys.Escape);
            InputHandler.AllKeysHandling[Keys.Escape].KeyDownActions.Add(game.Exit);

            game.Run();
        }
    }

    public static void ToggleCursorHide()
    {
        Screen.ShowCursor = !Screen.ShowCursor;
        InputHandler.LockTheMouse = !InputHandler.LockTheMouse;
    }
}