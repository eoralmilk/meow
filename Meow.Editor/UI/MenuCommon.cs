﻿using Myra;
using Myra.Graphics2D.UI;
using Myra.Graphics2D.UI.ColorPicker;
using Myra.Graphics2D.UI.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Editor.UI;

internal class MenuCommon : HorizontalMenu
{
    Editor _editor;

    public MenuCommon(Editor editor)
    {
        _editor = editor;

        _menuItemOpenFile = new MenuItem();
        _menuItemOpenFile.Text = "&Open";
        _menuItemOpenFile.ShortcutText = "Ctrl+O";
        _menuItemOpenFile.Id = "_menuItemOpenFile";

        _menuItemSaveFile = new MenuItem();
        _menuItemSaveFile.Text = "&Save";
        _menuItemSaveFile.ShortcutText = "Ctrl+S";
        _menuItemSaveFile.Id = "_menuItemSaveFile";

        _menuItemChooseFolder = new MenuItem();
        _menuItemChooseFolder.Text = "Choose Fol&der";
        _menuItemChooseFolder.ShortcutText = "Ctrl+D";
        _menuItemChooseFolder.Id = "_menuItemChooseFolder";

        //_menuItemChooseColor = new MenuItem();
        //_menuItemChooseColor.Text = "Choose Co&lor";
        //_menuItemChooseColor.ShortcutText = "Ctrl+L";
        //_menuItemChooseColor.Id = "_menuItemChooseColor";

        var menuSeparator1 = new MenuSeparator();

        _menuItemQuit = new MenuItem();
        _menuItemQuit.Text = "&Quit";
        _menuItemQuit.ShortcutText = "Ctrl+Q";
        _menuItemQuit.Id = "_menuItemQuit";

        _menuFile = new MenuItem();
        _menuFile.Text = "&File";
        _menuFile.Id = "_menuFile";
        _menuFile.Items.Add(_menuItemOpenFile);
        _menuFile.Items.Add(_menuItemSaveFile);
        _menuFile.Items.Add(_menuItemChooseFolder);
        //_menuFile.Items.Add(_menuItemChooseColor);
        _menuFile.Items.Add(menuSeparator1);
        _menuFile.Items.Add(_menuItemQuit);

        _menuItemCopy = new MenuItem();
        _menuItemCopy.Text = "&Copy";
        _menuItemCopy.ShortcutText = "Ctrl+Insert, Ctrl+C";
        _menuItemCopy.Id = "_menuItemCopy";

        _menuItemPaste = new MenuItem();
        _menuItemPaste.Text = "&Paste";
        _menuItemPaste.ShortcutText = "Shift+Insert, Ctrl+V";
        _menuItemPaste.Id = "_menuItemPaste";

        var menuSeparator2 = new MenuSeparator();

        _menuItemUndo = new MenuItem();
        _menuItemUndo.Text = "&Undo";
        _menuItemUndo.ShortcutText = "Ctrl+Z";
        _menuItemUndo.Id = "_menuItemUndo";

        _menuItemRedo = new MenuItem();
        _menuItemRedo.Text = "&Redo";
        _menuItemRedo.ShortcutText = "Ctrl+Y";
        _menuItemRedo.Id = "_menuItemRedo";

        _menuEdit = new MenuItem();
        _menuEdit.Text = "&Edit";
        _menuEdit.Id = "_menuEdit";
        _menuEdit.Items.Add(_menuItemCopy);
        _menuEdit.Items.Add(_menuItemPaste);
        _menuEdit.Items.Add(menuSeparator2);
        _menuEdit.Items.Add(_menuItemUndo);
        _menuEdit.Items.Add(_menuItemRedo);

        _menuItemAbout = new MenuItem();
        _menuItemAbout.Text = "&About";
        _menuItemAbout.Id = "_menuItemAbout";

        _menuHelp = new MenuItem();
        _menuHelp.Text = "&Help";
        _menuHelp.Id = "_menuHelp";
        _menuHelp.Items.Add(_menuItemAbout);

        VerticalAlignment = Myra.Graphics2D.UI.VerticalAlignment.Stretch;
        Id = "_mainMenu";
        Items.Add(_menuFile);
        Items.Add(_menuEdit);
        Items.Add(_menuHelp);



        _menuItemOpenFile.Image = DefaultAssets.UITextureRegionAtlas["icon-folder"];
        _menuItemSaveFile.Image = DefaultAssets.UITextureRegionAtlas["icon-folder-new"];

        //_menuItemOpenFile.Selected += (s, a) => OpenFile();
        //_menuItemSaveFile.Selected += (s, a) => SaveFile();
        //_menuItemChooseColor.Selected += (s, a) => ChooseColor();
        //_menuItemChooseFolder.Selected += (s, a) => ChooseFolder();
        _menuItemQuit.Selected += (s, a) => Quit();
    }



    //public void OpenFile()
    //{
    //    var fileDialog = new FileDialog(FileDialogMode.OpenFile);
    //    fileDialog.ShowModal(Desktop);

    //    fileDialog.Closed += (s, a) =>
    //    {
    //        if (!fileDialog.Result)
    //        {
    //            return;
    //        }

    //        _textOpenFile.Text = fileDialog.FilePath;
    //    };
    //}

    //public void SaveFile()
    //{
    //    var fileDialog = new FileDialog(FileDialogMode.SaveFile);
    //    fileDialog.ShowModal(Desktop);

    //    fileDialog.Closed += (s, a) =>
    //    {
    //        if (!fileDialog.Result)
    //        {
    //            return;
    //        }

    //        _textSaveFile.Text = fileDialog.FilePath;
    //    };
    //}

    //public void ChooseFolder()
    //{
    //    var fileDialog = new FileDialog(FileDialogMode.ChooseFolder);
    //    fileDialog.ShowModal(Desktop);

    //    fileDialog.Closed += (s, a) =>
    //    {
    //        if (!fileDialog.Result)
    //        {
    //            return;
    //        }

    //        _textChooseFolder.Text = fileDialog.FilePath;
    //    };
    //}

    //public void ChooseColor()
    //{
    //    var colorWindow = new ColorPickerDialog();
    //    colorWindow.Color = _textButtonLabel.TextColor;
    //    colorWindow.ShowModal(Desktop);

    //    colorWindow.Closed += (s, a) =>
    //    {
    //        if (!colorWindow.Result)
    //        {
    //            return;
    //        }

    //        _textButtonLabel.TextColor = colorWindow.Color;
    //    };
    //}

    public void Quit()
    {
        _editor.Exit();
    }

    public MenuItem _menuItemOpenFile;
    public MenuItem _menuItemSaveFile;
    public MenuItem _menuItemChooseFolder;
    public MenuItem _menuItemChooseColor;
    public MenuItem _menuItemQuit;
    public MenuItem _menuFile;
    public MenuItem _menuItemCopy;
    public MenuItem _menuItemPaste;
    public MenuItem _menuItemUndo;
    public MenuItem _menuItemRedo;
    public MenuItem _menuEdit;
    public MenuItem _menuItemAbout;
    public MenuItem _menuHelp;
}
