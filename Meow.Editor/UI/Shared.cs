﻿using Microsoft.Xna.Framework;
using Myra.Graphics2D.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Editor.UI;

public interface IEditorWorker
{
    public void Update(GameTime gameTime);

    public void Draw(GameTime gameTime, GraphicsDeviceManager gdm);
}


public static class UiUtils
{
    public static void SetRowCol(this Widget widget, int row, int col)
    {
        Grid.SetColumn(widget, col);
        Grid.SetRow(widget, row);
    }
}