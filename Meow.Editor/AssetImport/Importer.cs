﻿using Meow.Core;
using Meow.Editor.Spatial;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;


namespace Meow.Editor.AssetImport;

public enum ImportState
{
    None = 0,
    FileNotExist = 1,

    Succeed = 99,
}


public enum VertexContent : int
{
    Position = 1 << 0,
    Normal = 1 << 1,
    TangentBasis = 1 << 2,
    BoneWeights = 1 << 3,

    Texcoord1 = 1 << 4,
    Texcoord2 = 1 << 5,
    Texcoord3 = 1 << 6,
    Texcoord4 = 1 << 7,


    Color1 = 1 << 8,
    Color2 = 1 << 9,
    Color3 = 1 << 10,
    Color4 = 1 << 11,
}

public class RawMesh
{
    public Node3D Root;

    public Vector3[] Positions;
    public Vector3[] Normals;
    public Vector3[] Tangents;
    public Vector3[] BiTangents;

    public RawFace[] Faces;


    public Dictionary<string, System.Numerics.Matrix4x4> SkinnedBones;
    public List<RawWeight>[] BoneWeights;

    public RawMesh(Node3D root)
    {
        Root = root;
    }
}

public struct RawFace
{
    public int[] Indices;
}

public struct RawWeight
{
    public string Name;
    public float Weight;
}

public class RawSkeleton
{
    public Node3D Root;
    public HashSet<Node3D> BoneNodes;
    public Dictionary<Node3D, Matrix> BindOffset;
}

public static class ImportUtils
{

    public static bool VertexContains(int mask, VertexContent vertexContentType) { return (mask & (int)VertexContent.Normal) != 0; }

    public static System.Numerics.Matrix4x4 ToNumeric(in Assimp.Matrix4x4 matrix)
    {
        var result = System.Numerics.Matrix4x4.Identity;

        result.M11 = matrix.A1;
        result.M12 = matrix.B1;
        result.M13 = matrix.C1;
        result.M14 = matrix.D1;

        result.M21 = matrix.A2;
        result.M22 = matrix.B2;
        result.M23 = matrix.C2;
        result.M24 = matrix.D2;

        result.M31 = matrix.A3;
        result.M32 = matrix.B3;
        result.M33 = matrix.C3;
        result.M34 = matrix.D3;

        result.M41 = matrix.A4;
        result.M42 = matrix.B4;
        result.M43 = matrix.C4;
        result.M44 = matrix.D4;

        return result;
    }

    public static System.Numerics.Matrix4x4 ToNumeric(this Assimp.Matrix4x4 matrix)
    {
        var result = System.Numerics.Matrix4x4.Identity;

        result.M11 = matrix.A1;
        result.M12 = matrix.B1;
        result.M13 = matrix.C1;
        result.M14 = matrix.D1;

        result.M21 = matrix.A2;
        result.M22 = matrix.B2;
        result.M23 = matrix.C2;
        result.M24 = matrix.D2;

        result.M31 = matrix.A3;
        result.M32 = matrix.B3;
        result.M33 = matrix.C3;
        result.M34 = matrix.D3;

        result.M41 = matrix.A4;
        result.M42 = matrix.B4;
        result.M43 = matrix.C4;
        result.M44 = matrix.D4;

        return result;
    }

    public static Vector3 FromAssimp(Assimp.Vector3D vector3D) { return new Vector3(vector3D.X, vector3D.Y, vector3D.Z); }
}