﻿using Arch.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.Simulation;

public interface IUpdateSystem
{
    void Update(GameTime gameTime, float totalElapsedTime, float scaleFactor);
}
public interface ITickSystem
{
    void Tick(int tick);
}

public interface IRenderSystem
{
    abstract void Render(GraphicsDeviceManager gdm, GameTime gameTime, float totalElapsedTime, int tick);
}

public abstract class ECS_System
{
    protected World world;
    protected Scene scene;

    public ECS_System(World world, Scene scene)
    {
        this.world = world;
        this.scene = scene;
    }
}