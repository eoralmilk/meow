﻿using Meow.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.Simulation;

public struct WithMesh
{
    public Vector3 Offset;
    public IMesh mesh;
    public MatInstance material;
}
