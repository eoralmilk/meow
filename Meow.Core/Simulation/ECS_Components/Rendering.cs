
using System.Numerics;

namespace Meow.Core.Simulation;

public struct C_Camera
{
    public Vector3 Position;
    public Quaternion Rotation;
    public Vector3 Up;

    public float Fov;
    public float Near;
    public float Far;
    public float AspectRatio;

    public Matrix4x4 View;
    public Matrix4x4 Projection;

    //// avoid the implicit conversion
    //public Microsoft.Xna.Framework.Vector3 PositionRender;
    //public Microsoft.Xna.Framework.Matrix ViewRender;
    //public Microsoft.Xna.Framework.Matrix ProjectionRender;

    ///// <summary>
    ///// We do the conversion manually before rendering
    ///// </summary>
    //public void ConvertMatrixForRender()
    //{
    //    PositionRender = Position;
    //    ViewRender = View;
    //    ProjectionRender = Projection;
    //}
}

public struct C_CameraRenderInfo
{
    public Microsoft.Xna.Framework.Vector3 PositionRender;
    public Microsoft.Xna.Framework.Matrix ViewRender;
    public Microsoft.Xna.Framework.Matrix ProjectionRender;
}

public struct C_DirectionLight
{
    public Vector3 Direction;
    public Vector3 Flux;
    public bool Shadow;
}

public struct C_PointLight
{
    public Vector3 Position;
    public Vector3 Flux;
    public bool Shadow;
}

public struct C_AmbientLight
{
    public Vector3 Irradiance;
}


public struct C_Renderable
{
    public Microsoft.Xna.Framework.BoundingSphere BoundingSphere;
    public Microsoft.Xna.Framework.BoundingSphere LocalBoundingSphere;

    public bool ToDraw;
    public bool ToCastShadow;
}