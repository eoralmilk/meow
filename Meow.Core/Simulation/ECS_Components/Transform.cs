using System.Numerics;

namespace Meow.Core.Simulation;

public struct C_Position
{
    public Vector3 Value;
}

public struct C_Scale
{
    public Vector3 Value;
}

public struct C_Rotation
{
    public Quaternion Value;
}

