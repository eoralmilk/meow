﻿using Arch.Core;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.Simulation;

public class ShadowMapRenderSys : ECS_System , IRenderSystem
{
    QueryDescription queryDirLights = new QueryDescription().WithAll<C_DirectionLight>();

    public ShadowMapRenderSys(World world, Scene scene) : base(world, scene)
    {
    }

    void IRenderSystem.Render(GraphicsDeviceManager gdm, GameTime gameTime, float totalElapsedTime, int tick)
    {
        world.Query(in queryDirLights, (ref C_DirectionLight light) =>
        {
            if (!light.Shadow)
                return;

            // TODO: shadow map render
        });

    }
}
