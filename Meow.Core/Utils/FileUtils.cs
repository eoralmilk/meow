﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Meow.Core.Utils;
public static class FileUtils
{
    public static JObject LoadJson(string path)
    {
        if (File.Exists(path))
            return JObject.Parse(File.ReadAllText(path));

        return null;
    }

    public static void SaveJson(string path, object obj)
    {
        var jObject = JObject.FromObject(obj);

        File.WriteAllText(path, jObject.ToString());
    }

    public static T LoadValue<T>(this JToken json, in string key, T fallback)
    {
        return json[key] == null ? fallback : json[key].ToObject<T>();
    }
}