﻿using Microsoft.Xna.Framework;

namespace Meow.Core.Input;

public static class MouseUtil
{
    /// <returns>Returns true when the scroll wheel is scrolled.</returns>
    public static bool Scrolled => ScrollDelta != 0;
    /// <returns>Returns the difference between last frame and this frame's scroll wheel value.</returns>
    public static int ScrollDelta => InputHandler.NewMouse.ScrollWheelValue - InputHandler.OldMouse.ScrollWheelValue;

    /// <returns>Returns true when the mouse pointer is moved.</returns>
    public static bool PointerMoved => PointerDelta != Point.Zero;
    ///<returns>Returns the difference between the last frame and this frame's mouse pointer position.</returns>
    public static Point PointerDelta => InputHandler.NewMouse.Position - InputHandler.OldMouse.Position;

    public static bool IsMouseValid =>
            0 <= InputHandler.NewMouse.X && InputHandler.NewMouse.X <= InputHandler.WindowWidth &&
            0 <= InputHandler.NewMouse.Y && InputHandler.NewMouse.Y <= InputHandler.WindowHeight;
}