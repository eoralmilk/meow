﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core
{
    public static class Debug
    {
        public static void Log(params object[] args)
        {
#if DEBUG
            if (args.Length > 1)
            {
                for (int i = 0, l = args.Length; i < l; i++)
                    System.Console.WriteLine(args[i]);
            }
            else
                System.Console.WriteLine(args[0]);
#endif
        }

        public static void LogFormat(string format, params object[] args)
        {
#if DEBUG
            System.Console.WriteLine(string.Format(format, args));
#endif
        }
    }
}
