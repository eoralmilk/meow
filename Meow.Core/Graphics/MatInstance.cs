﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.Graphics
{
    abstract public class MatInstance : IDisposable
    {
        public readonly MaterialDefine materialDefine;

        public MatInstance(MaterialDefine materialDefine)
        {
            this.materialDefine = materialDefine;
        }

        public abstract void FronJson(JToken json);

        public abstract void SaveJson(in string path);

        public abstract void PrePass(in Vector3 cameraPosition, in Matrix viewMatrix, in Matrix projectionMatrix);
        public abstract void PrePassForward(in Vector3 cameraPosition, in Matrix viewMatrix, in Matrix projectionMatrix);
        public abstract void Pass(in Matrix worldMatrix, bool receiveShadow, bool drawInstanced);

        public abstract void Dispose();
    }
}
