﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Meow.Core.Graphics
{
    abstract public class MaterialDefine
    {
        internal protected Effect _effect;

        public virtual void LoadContent(ContentManager content)
        {
            SetupShaderMaterial(content);
        }

        /// <summary>
        /// should create the effect in local, we can handle it by ourself
        /// the effect should be dispose when the scene is disposing
        /// </summary>
        protected abstract void SetupShaderMaterial(ContentManager content);

        public virtual void Dispose() 
        {
            _effect.Dispose();
        }
    }
}
