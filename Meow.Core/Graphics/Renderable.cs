﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Meow.Core.Simulation;

namespace Meow.Core.Graphics
{
    abstract public class Renderable : IComparable
    {
        internal protected BoundingSphere boundingSphere;

        public bool CastShadow { get; set; } = true;

        public virtual bool InstancedEnabled { get; }

        public bool ReceiveShadow { get; set; } = true;

        public BoundingSphere BoundingSphere => boundingSphere;

        public int RenderQueue { get; protected set; } = 1;

        public bool SortForBlend;

        public Renderable()
        {
        }

        /// <summary>
        /// Draw the content of the component.
        /// </summary>
        /// <param name="device"></param>
        public abstract void Draw(in C_CameraRenderInfo camera, GraphicsDeviceManager gdm);

        public virtual int CompareTo(object obj)
        {
            var renderable = obj as Renderable;

            if (renderable == null)
                return 1;

            if (SortForBlend == renderable.SortForBlend)
                return 0;
            else if (SortForBlend && !renderable.SortForBlend)
                return 1;
            else
                return -1;
        }
    }
}
