﻿using Meow.Core.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.Graphics
{
    public class UnlitMaterial : MaterialDefine
    {
        protected override void SetupShaderMaterial(ContentManager content)
        {
            _effect = content.LoadLocalized<Effect>("CoreAssets/Shaders/Forward/Unlit");
        }

       
    }


    public class Mti_Basic3d : MatInstance
    {
        public Vector2 Tiling;

        public Texture2D MainTexture;
        public Vector3 DiffuseColor;

        public float Cutout;

        //public Texture2D AddingTexture; // r == SpecularIntensity, g == SpecularPower / 256, b == ?, a == EmissiveIntensity


        //public Vector3 SpecularColor;
        ////public int SpecularPower; // 控制高光区域大小
        //public float SpecularIntensity;

        public Mti_Basic3d(MaterialDefine materialDefine) : base(materialDefine)
        {
            Tiling = Vector2.One;
            DiffuseColor = Color.White.ToVector3();
        }

        public override void FronJson(JToken json)
        {
            throw new NotImplementedException();
        }

        public override void SaveJson(in string path)
        {
            throw new NotImplementedException();
        }

        public override void PrePass(in Vector3 cameraPosition, in Matrix viewMatrix, in Matrix projectionMatrix)
        {
            throw new NotImplementedException();
        }

        public override void PrePassForward(in Vector3 cameraPosition, in Matrix viewMatrix, in Matrix projectionMatrix)
        {
            materialDefine._effect.Parameters["View"].SetValue(viewMatrix);
            materialDefine._effect.Parameters["Projection"].SetValue(projectionMatrix);
        }

        public override void Pass(in Matrix worldMatrix, bool receiveShadow, bool drawInstanced)
        {
            materialDefine._effect.Parameters["World"].SetValue(worldMatrix);
            materialDefine._effect.Parameters["TextureTiling"].SetValue(Tiling);
            materialDefine._effect.Parameters["DiffuseColor"].SetValue(DiffuseColor);
            materialDefine._effect.Parameters["MainTexture"].SetValue(MainTexture);
            materialDefine._effect.Parameters["Cutout"].SetValue(Cutout);

            materialDefine._effect.CurrentTechnique.Passes[MainTexture == null ? 0 : 1].Apply(); ;
        }

        public override void Dispose()
        {
        }
    }
}
