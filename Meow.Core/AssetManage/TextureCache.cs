﻿using Meow.Core.Graphics;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
namespace Meow.Core.AssetManage;

public class TextureCache : IAssetCache
{
    public Texture2D FallbackTexture { get; protected set; }
    internal readonly Dictionary<string, Texture> TextureDictionary = new Dictionary<string, Texture>();

    public TextureCache()
    {

    }

    public void Init()
    {
        FallbackTexture = Application.Content.Load<Texture2D>("CoreAssets/Img/rumya_labmeow");
    }

    public void AddTexture(in string keyName, Texture tex)
    {
        TextureDictionary.Add(keyName, tex);
    }

    public Texture TryGetTexture(in string keyName)
    {
        if (TextureDictionary.TryGetValue(keyName, out Texture tex))
            return tex;

        return FallbackTexture;
    }

    public void Dispose()
    {
        foreach (var (key, tex) in TextureDictionary)
            tex.Dispose();

        FallbackTexture.Dispose();
    }

}
