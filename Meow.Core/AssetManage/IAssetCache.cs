﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.AssetManage;

public interface IAssetCache : IDisposable
{
    /// <summary>
    /// Load FallBack Assets can go here
    /// </summary>
    public void Init();

}
