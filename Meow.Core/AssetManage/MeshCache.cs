﻿using Meow.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core.AssetManage;

public class MeshCache : IAssetCache
{
    public IMesh FallbackMesh { get; protected set; }
    public readonly Dictionary<int, IMesh> MeshDictionary = new Dictionary<int, IMesh>();

    public MeshCache()
    {

    }

    public void Init()
    {
        FallbackMesh = new CubeMesh();
        FallbackMesh.Build();
    }

    public IMesh TryGetMesh(int id)
    {
        if (MeshDictionary.TryGetValue(id, out IMesh mesh))
            return mesh;

        return FallbackMesh;
    }

    public void Dispose()
    {
        foreach (var (key, mesh) in MeshDictionary)
            mesh.Dispose();

        FallbackMesh.Dispose();
    }
}
