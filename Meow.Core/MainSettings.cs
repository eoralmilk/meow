﻿using Meow.Core.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meow.Core;

public class MainSettings
{
    public string MainScene { get; set; }

    // Video
    public int WindowWidth { get; set; }
    public int WindowHeight { get; set; }
    public bool FullScreen { get; set; }

    /// <summary>
    /// 0 will turn of msaa
    /// </summary>
    public int MSAA { get; set; }
    public bool SynchronizeWithVerticalRetrace { get; set; }

    // Audio
    public float MasterVolume { get; set; }
    public float MenuMusicVolume { get; set; }
    public float GameMusicVolume { get; set; }
    public float SoundEffectsVolume { get; set; }

    public MainSettings()
    {
        MainScene = "";
        WindowWidth = 640;
        WindowHeight = 480;
        FullScreen = false;
        MSAA = 4;
        SynchronizeWithVerticalRetrace = true;
        MasterVolume = 0.5f;
        MenuMusicVolume = 1f;
        GameMusicVolume = 1f;
        SoundEffectsVolume = 1f;
    }

    public void Save(in string path)
    {
        var jObject = JObject.FromObject(this);

        File.WriteAllText(path, jObject.ToString());
    }

    public static MainSettings FromJson(JToken settings)
    {
        var container = new MainSettings();
        container.MainScene = settings.LoadValue("MainScene", "");

        // Video
        container.WindowWidth = settings.LoadValue("WindowWidth", 640);
        container.WindowHeight = settings.LoadValue("WindowHeight", 480);
        container.FullScreen = settings.LoadValue("FullScreen", false);
        container.MSAA = settings.LoadValue("MSAA", 0);
        container.SynchronizeWithVerticalRetrace = settings.LoadValue("SynchronizeWithVerticalRetrace", true);

        // Audio
        container.MasterVolume = settings.LoadValue("MasterVolume", 0.5f);
        container.MenuMusicVolume = settings.LoadValue("MenuMusicVolume", 1f);
        container.GameMusicVolume = settings.LoadValue("GameMusicVolume", 1f);
        container.SoundEffectsVolume = settings.LoadValue("SoundEffectsVolume", 1f);
        return container;
    }
}
