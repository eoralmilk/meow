# Meow
## 喵呜 —— 基于MonoGame开发的轻量型跨平台3d游戏引擎.

<img src="./dr_rurumeow.png">


### - 采用ECS架构：
- 面向数据编程缓存命中率高，性能优秀，并且非常适合并行处理业务逻辑。
- 帧同步基础稳定易实现。（未来计划）

### - Scene场景化设计:
- 切换场景方便简单，结构易于理解。
- 使用类似通用引擎的设计理念，可拓展性极强。



## Meow尚在早期开发中
## WIP

## FAQ
1. 没法构建mgcb:  
    - 可能丢失了.config文件夹。参考Meow.Core中的.config文件夹
